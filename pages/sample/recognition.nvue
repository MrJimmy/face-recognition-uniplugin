// 人脸识别的 demo
//  特别注意：算法只需要全局初始化一次(initFacePass)，初始化成功后调用在开始检测人脸和识别人脸
<template>
	<view>
		<ES-FacePass-ESRecognitionComponent ref="faceRecognition" style="width:800;height:1280" />
			<view class="buttons">
				<button class="btn" type="primary" @click="openCamera()">打开相机</button>
				<button class="btn" type="primary" @click="closeCamera()">关闭相机</button>
				<button class="btn" type="primary" @click="startRecognition()">开始识别</button>
				<button class="btn" type="primary" @click="start1V1Recognition()">开始1V1识别</button>
				<button class="btn" type="primary" @click="stopRecogniton()">停止识别</button>
			</view>
			
			<view class="buttonsTwo">
				<button class="btn" type="primary" @click="getRecognitionSuccessPhoto()">获取成功照片</button>
				<button class="btn" type="primary" @click="getRecognitionFailurePhoto()">获取失败照片</button>
				<button class="btn" type="primary" @click="getLivingFailurePhoto()">获取活体失败照片</button>
				<button class="btn" type="primary" @click="getFaceDetectPhoto()">获取人脸检测照片</button>
				<button class="btn" type="primary" @click="clearRecognitionPhoto()">清除所有照片</button>
			</view>
			
			<view class="state-views">
				<text class="state-text">{{detectFaceResult}}</text>
				<text class="state-text">{{recognitionResult}}</text>
			</view>
		</view>
	</view>
</template>

<script>
	import {
		pathToBase64,
		base64ToPath
	} from '../../js_sdk/mmmm-image-tools/index.js'

	// 获取 module
	const facePassModule = uni.requireNativePlugin('ES-FacePass-ESFacePassModule');
	export default {
		data() {
			return {
				detectFaceResult: "",
				recognitionResult: "",
				successPhotoKey: "",
				failurePhotoKey: "",
				livingPhotoKey: ""
			}
		},
		onBackPress() {
			this.$refs.faceRecognition.closeCamera();
			this.$refs.faceRecognition.stopFaceDetect();
			this.$refs.faceRecognition.stopFaceRecognition();
			//  清理图片按需
			facePassModule.clearRecognitionPhoto();
			return false
		},
		methods: {
			openCamera() {
				//  打开相机
				this.$refs.faceRecognition.openCamera(ret => {
					if (ret == 401) {
						console.error('相机打开成功');
						
						//  相机打开成功后开始检测人脸
						this.$refs.faceRecognition.startFaceDetect({"isSaveDetectPhoto":true}, ret => {
							this.$options.methods.refreshFaceDetectState(this, ret)
						});
						
					} else if (ret == 402) {
						console.error('相机打开失败');
					}
				});
				//  开始预览
				this.$refs.faceRecognition.startPreview()
			},
			closeCamera() {
				//  关闭相机
				this.$refs.faceRecognition.closeCamera();
			},
			startRecognition() {
				//  开始识别人脸，需要算法初始化成功，并且开始检测人脸了
				this.$refs.faceRecognition.startFaceRecognition({},ret => {
					this.$options.methods.refreshFaceRecognitionResult(this, ret)
				});
			},
			start1V1Recognition() {
				//  使用插件 https://ext.dcloud.net.cn/plugin?id=123#detail 把本地图片转换为 base64
				pathToBase64('/static/test_1v1.jpeg')
					.then(base64 => {
						//  开始 1:1 人脸 比对，需要算法初始化成功，并且开始检测人脸了
						this.$refs.faceRecognition.start1V1Recognition({
							"searchThreshold": 65,
							"photo": base64
						}, ret => {
							this.$options.methods.refreshFaceRecognitionResult(this, ret)
						});
					})
					.catch(error => {
						console.error(error)
					})
			},
			stopRecogniton() {
				this.$refs.faceRecognition.stopFaceRecognition();
			},
			getRecognitionSuccessPhoto(){
				if(this.successPhotoKey != ""){
					const photoBase64 = facePassModule.getRecognitionSuccessPhoto(this.successPhotoKey);
					console.error(`getRecognitionSuccessPhoto：${photoBase64}` )
				}
			},
			getRecognitionFailurePhoto(){
				if(this.failurePhotoKey != ""){
					const photoBase64 = facePassModule.getRecognitionFailurePhoto(this.failurePhotoKey);
					console.error(`getRecognitionFailurePhoto：${photoBase64}` )
				}
			},
			getLivingFailurePhoto(){
				if(this.livingPhotoKey != ""){
					const photoBase64 = facePassModule.getLivingFailurePhoto(this.livingPhotoKey);
					console.error(`getLivingFailurePhoto：${photoBase64}` )
				}
			},
			getFaceDetectPhoto(){
				const photoBase64 = facePassModule.getFaceDetectSuccessPhoto();
				console.error(`getFaceDetectSuccessPhoto：${photoBase64}` )
			},
			clearRecognitionPhoto(){
				const clearResult = facePassModule.clearRecognitionPhoto();
				console.error(`clearRecognitionPhoto: ${clearResult}`)
			},
			//  处理人脸检测结果
			refreshFaceDetectState(_this, ret) {
				if (ret == 201) {
					_this.detectFaceResult = "无人脸"
				} else if (ret == 202) {
					_this.detectFaceResult = "存在人脸"
				}
			},
			refreshFaceRecognitionResult(_this, ret) {
				const json = JSON.parse(JSON.stringify(ret));
				const code = json["code"];
				if (code == 301) {
					// 识别成功
					//  识别成功的相似度
					const searchScore = json["searchScore"];
					//  识别成功的照片
					const photoKey = json["photoKey"];
					//  是否是 1:1  比对
					const is1V1 = json["is1V1"];
					//  识别成功的 FaceToken，非 1:1  比对才有用
					const faceToken = json["faceToken"];
					_this.recognitionResult = `识别成功，相似度:${searchScore}`
					
					_this.successPhotoKey = photoKey
					console.error(`识别成功，相似度:${searchScore}  photoKey:${photoKey}`)
				} else if (code == 302) {
					// 识别失败
					//  与底库比对获取到最高的相似度
					const searchScore = json["searchScore"];
					//  识别的照片
					const photoKey = json["photoKey"];
					//  是否是 1:1  比对
					const is1V1 = json["is1V1"];
					
					_this.failurePhotoKey = photoKey
					_this.recognitionResult = `识别失败， 相似度:${searchScore}`
					console.error(`识别失败， 相似度:${searchScore}  photoKey:${photoKey}`)
				} else if (code == 303) {
					// 活体验证不通过

					//  活体得分
					const livenessScore = json["livenessScore"];
					//  识别的照片
					const photoKey = json["photoKey"];
					//  是否是 1:1  比对
					const is1V1 = json["is1V1"];
					
					_this.livingPhotoKey = photoKey
					_this.recognitionResult = `活体检测失败， 活体得分:${livenessScore}`
					
					console.error(`活体检测失败， 活体得分:${livenessScore}  photoKey:${photoKey}`)
				}
			}
		}
	}
</script>

<style>
	@import url("../../common/custom.css");
	.buttons {
		width: wrap;
		height: wrap;
		position: absolute;
		flex-direction: row;
	}
	.buttonsTwo {
		width: wrap;
		height: wrap;
		position: absolute;
		flex-direction: row;
		top: 120rpx;
	}
	.state-views {
		width: wrap;
		height: wrap;
		position: absolute;
		flex-direction: column;
		top: 240rpx;
		left: 20rpx;
	}
	.state-text {
		color: red;
		font-size: 20upx;
	}
	
</style>